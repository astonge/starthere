var express = require('express');
var path = require('path');

// Application setup
var app = express();
app.set('view engine','pug');
app.use('/css', express.static(path.join(__dirname,'public/css/')));
app.use('/bootstrap', express.static(path.join(__dirname,'node_modules/bootstrap/dist/')));
app.use('/jquery', express.static(path.join(__dirname,'node_modules/jquery/dist/')));

// Routes & Views
app.get('/', function(req,res) {
  res.render('index', {title:'Start Here',message:'Testing'});
});

// Start Sever
var server = app.listen(8080, function() {
  var host = server.address().address;
  var port = server.address().port;
  console.log('Start Here v0.0 Running');
});
module.exports = server;
